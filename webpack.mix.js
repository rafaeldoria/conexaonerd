let mix = require('laravel-mix');
require('laravel-mix-clean');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.clean({
        cleanOnceBeforeBuildPatterns: [
            './js/*',
            './css/*',
            './blog',
            './admin'
        ],
        verbose:true
});

mix.js('assets/admin/js/app.js', 'public/js')
    .postCss('assets/admin/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
]).postCss('assets/admin/css/adminapp.css', 'public/css')
    .styles('assets/blog/css/all.css','public/blog/css/all.css')
    .postCss('assets/blog/css/responsive/responsive.css','public/blog/css/responsive/responsive.css')
    .styles(['assets/blog/css/others/animate.css',
        'assets/blog/css/others/magnific-popup.css',
        'assets/blog/css/others/pe-icon-7-stroke.css'
    ], 'public/blog/css/others.css')
    .styles(['assets/blog/css/others/meanmenu.min.css',
        'assets/blog/css/others/owl.carousel.min.css',
        'assets/blog/css/others/font-awesome.min.css'
    ], 'public/blog/css/others.min.css')
    .copy('assets/blog/css/bootstrap/bootstrap.min.css',
        'public/blog/css')
    .copy('assets/blog/css/style.css', 'public/blog/css/')
    .copy('assets/blog/js/jquery-2.2.4.min.js', 'public/blog/js/jquery')
    .copy('assets/blog/js/popper.min.js', 'public/blog/js/bootstrap')
    .copy('assets/blog/js/bootstrap.min.js', 'public/blog/js/bootstrap')
    .copy('assets/blog/js/plugins.js', 'public/blog/js/others/plugins.js')
    .copy('assets/blog/js/active.js', 'public/blog/js/active.js')
    .copy('assets/blog/fonts', 'public/blog/fonts')
    .copy('assets/blog/img', 'public/blog/img')
    .copy('assets/admin/img', 'public/admin/img');