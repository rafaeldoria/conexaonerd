<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleType extends Model
{
    public const _INACTIVE = 0;
    public const _ACTIVE  = 1;

    use HasFactory;

    protected $table = 'article_types';

    protected $attributes = [
        'active' => self::_INACTIVE
    ];

    protected $fillable = ['title', 'description', 'active', 'file_path', 'file_name'];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public static function getAll()
    {
        $articleTypes = articleType::where('active', self::_ACTIVE)
            ->whereNull('deleted_at')
            ->get();

        return $articleTypes;
    }
    
}
