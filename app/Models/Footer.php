<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    public const _INACTIVE = 0;
    public const _ACTIVE  = 1;

    use HasFactory;

    protected $table = 'footers';

    protected $fillable = ['name', 'url', 'file_path', 'user', 'active'];
}
