<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Author extends Model
{
    public const _INACTIVE = 0;
    public const _ACTIVE  = 1;

    use HasFactory;

    protected $table = 'authors';

    protected $attributes = [
        'active' => self::_INACTIVE
    ];

    protected $fillable = ['first_name', 'last_name', 'description', 'file_path', 'file_name' , 'active', 'user_id', ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
