<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public const _INACTIVE = 0;
    public const _ACTIVE  = 1;
    public const _ARTICLE_TYPE_DEFAULT  = 1;

    use HasFactory;

    protected $table = 'articles';

    protected $attributes = [
        'active' => self::_INACTIVE
    ];

    protected $fillable = ['title', 'summary', 'text', 'file_path', 'file_name','article_type_id', 'active', 'author_id'];

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function articleType()
    {
        return $this->belongsTo(ArticleType::class);
    }

    public function capa()
    {
        return $this->hasOne(Capa::class);
    }
}
