<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Capa extends Model
{
    public const _INACTIVE = 0;
    public const _ACTIVE  = 1;

    use HasFactory;

    protected $table = 'capas';

    protected $fillable = ['name', 'file_path', 'user', 'active'];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
