<?php

namespace App\Transformers;

use App\Models\Capa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CapaTransformer
{
    public const _INACTIVE = Capa::_INACTIVE;

    public static function save(Request $request) : Capa
    {
        $path = $request->file('file_path')->store('capas');
        $name = $request->file('file_path')->getClientOriginalName();

        $capa = new Capa;
        $capa->name = $name;
        $capa->article_id = $request->article_id;
        $capa->file_path = $path;
        $capa->user_id = Auth::id();
        $capa->active = $request->active;
        return $capa;
    }
}