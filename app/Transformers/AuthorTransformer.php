<?php

namespace App\Transformers;

use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthorTransformer
{
    public const _INACTIVE = Author::_INACTIVE;
    
    public static function save(Request $request) : Author
    {
        $author = new Author;
        if ($request->hasFile('file_path')) {
            $request->validate([
                'image' => 'mimes:jpeg,bmp,png'
            ]);
            $path = $request->file('file_path')->store('authors');
            $name = $request->file('file_path')->getClientOriginalName();
            $author->file_path = $path;
            $author->file_name = $name;
        }
        $author->first_name = $request->first_name;
        $author->last_name = $request->last_name;
        $author->description = $request->description;
        $author->active = isset($request->active) ? $request->active : self::_INACTIVE;
        $author->user_id = Auth::id();
        return $author;
    }
}
