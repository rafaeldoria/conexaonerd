<?php

namespace App\Transformers;

use App\Models\ArticleType;
use Illuminate\Http\Request;

class ArticleTypeTransformer
{
    public const _INACTIVE = ArticleType::_INACTIVE;

    public static function save(Request $request): ArticleType
    {
        $articleType = new ArticleType;
        if ($request->hasFile('file_path')) {
            $request->validate([
                'image' => 'mimes:jpeg,bmp,png'
            ]);
            $path = $request->file('file_path')->store('articleTypes');
            $name = $request->file('file_path')->getClientOriginalName();
            $articleType->file_path = $path;
            $articleType->file_name = $name;
        }
        $articleType->title = $request->title;
        $articleType->description = $request->description;
        $articleType->active = isset($request->active) ? $request->active : self::_INACTIVE;
        $articleType->save();

        return $articleType;
    }
}
