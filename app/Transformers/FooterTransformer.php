<?php

namespace App\Transformers;

use App\Models\Footer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FooterTransformer
{
    public const _INACTIVE = Footer::_INACTIVE;

    public static function save(Request $request) : Footer
    {
        $path = $request->file('file_path')->store('footers');
        $name = $request->file('file_path')->getClientOriginalName();

        $footer = new Footer;
        $footer->name = $name;
        $footer->url = $request->url;
        $footer->file_path = $path;
        $footer->user = Auth::id();
        $footer->active = $request->active;
        return $footer;
    }
}