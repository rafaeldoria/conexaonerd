<?php

namespace App\Transformers;

use App\Models\Article;
use App\Models\Author;
use Illuminate\Http\Request;

class ArticleTransformer
{
    public const _INACTIVE = Article::_INACTIVE;
    public const _ARTICLE_TYPE_DEFAULT = Article::_ARTICLE_TYPE_DEFAULT;

    public static function save(Request $request, Author $author): Article
    {
        $article = new Article;
        if ($request->hasFile('file_path')) {
            $request->validate([
                'image' => 'mimes:jpeg,bmp,png'
            ]);
            $path = $request->file('file_path')->store('articles');
            $name = $request->file('file_path')->getClientOriginalName();
            $article->file_path = $path;
            $article->file_name = $name;
        }
        $article->title = $request->title;
        $article->summary = $request->summary;
        $article->text = $request->text;
        $article->article_type_id = isset($request->article_type_id) ? $request->article_type_id : self::_ARTICLE_TYPE_DEFAULT;
        $article->active = isset($request->active) ? $request->active : self::_INACTIVE;
        $article->author_id = $author->id;

        return $article;
    }

}
?>