<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\IRepositories\AuthorIRepository', 'App\Repositories\AuthorRepository');
        $this->app->bind('App\Repositories\IRepositories\ArticleIRepository', 'App\Repositories\ArticleRepository');
        $this->app->bind('App\Repositories\IRepositories\ArticleTypeIRepository', 'App\Repositories\ArticleTypeRepository');
        $this->app->bind('App\Repositories\IRepositories\FooterIRepository', 'App\Repositories\FooterRepository');
        $this->app->bind('App\Repositories\IRepositories\CapaIRepository', 'App\Repositories\CapaRepository');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        if (App::environment() !== 'local') {
            $url->forceScheme('https');
        }
        $this->app->bind(
            AuthorHandler::class,
            ArticleHandler::class,
            ArticleTypeHandler::class,
            FooterHandler::class,
            Capa::class
        ); 
    }
}
