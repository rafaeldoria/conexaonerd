<?php

namespace App\Repositories;

use App\Models\Footer;
use App\Repositories\IRepositories\FooterIRepository;
use Illuminate\Contracts\Pagination\Paginator;

class FooterRepository implements FooterIRepository
{
    public const _LIMIT = 10;
    public const _ACTIVE = Footer::_ACTIVE;

    public function find(): Paginator
    {
        return Footer::whereNull('deleted_at')->paginate(10);
    }

    public function findActives(int $limit)
    {
        return Footer::whereNull('deleted_at')->where('active', self::_ACTIVE)->limit($limit)->orderByDesc('id')->get();
    }

    public function save(Footer $Footer)
    {
        return $Footer->save();
    }

    public function findById($id)
    {
        return Footer::where('id', $id)
            ->whereNull('deleted_at')
            ->first();
    }

    public function update(Footer $footer, $data)
    {
        $footer->update($data);
    }
}
