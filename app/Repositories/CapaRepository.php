<?php

namespace App\Repositories;

use App\Models\Capa;
use App\Repositories\IRepositories\CapaIRepository;
use Illuminate\Contracts\Pagination\Paginator;

class CapaRepository implements CapaIRepository
{
    public const _LIMIT = 6;
    public const _ACTIVE = Capa::_ACTIVE;

    public function find(): Paginator
    {
        return Capa::whereNull('deleted_at')->paginate(8);
    }

    public function findActives(int $limit)
    {
        return Capa::whereNull('deleted_at')->where('active', self::_ACTIVE)->limit($limit)->orderByDesc('id')->get();
    }

    public function save(Capa $capa)
    {
        return $capa->save();
    }

    public function findById($id)
    {
        return Capa::where('id', $id)
            ->whereNull('deleted_at')
            ->first();
    }

    public function update(Capa $capa, $data)
    {
        $capa->update($data);
    }
    public function delete(int $id)
    {
        Capa::where('id', $id)->delete();
    }
}
