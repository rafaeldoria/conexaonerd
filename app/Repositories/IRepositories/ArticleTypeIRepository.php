<?php

namespace App\Repositories\IRepositories;

use App\Models\ArticleType;

interface ArticleTypeIRepository
{
    public function find();
    public function findActives();
    public function findBy(int $id);
    public function save(ArticleType $article);
    public function update(ArticleType $article, $data);
}
