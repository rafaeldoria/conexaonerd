<?php

namespace App\Repositories\IRepositories;

use App\Models\Footer;

interface FooterIRepository
{
    public function find();
    public function findActives(int $limit);
    public function save(Footer $footer);
    public function findById(int $id);
    public function update(Footer $footer, $data);
}
