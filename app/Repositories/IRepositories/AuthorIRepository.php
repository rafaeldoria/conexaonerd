<?php

namespace App\Repositories\IRepositories;

use App\Models\Author;

interface AuthorIRepository
{
    public function find();
    public function findByIdByAuthor(int $id);
    public function findById(int $id);
    public function findByUser();
    public function save(Author $author);
    public function update(Author $author, $data);
}
?>