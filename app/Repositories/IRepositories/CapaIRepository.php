<?php

namespace App\Repositories\IRepositories;

use App\Models\Capa;

interface CapaIRepository
{
    public function find();
    public function findActives(int $limit);
    public function save(Capa $capa);
    public function findById(int $id);
    public function update(Capa $capa, $data);
    public function delete(int $id);
}
