<?php

namespace App\Repositories\IRepositories;

use App\Models\Article;

interface ArticleIRepository
{
    public function find();
    public function findById(int $id);
    public function findByAuthor(int $author_id);
    public function findByIdByAuthor(int $id, int $author_id);
    public function save(Article $article);
    public function update(Article $article, $data);
    public function findActives(int $limit);
    public function findActivesByTypeArticle($article_type_id, $limit);
}
