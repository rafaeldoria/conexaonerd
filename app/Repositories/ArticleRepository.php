<?php

namespace App\Repositories;

use App\Models\Article;
use App\Repositories\IRepositories\ArticleIRepository;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

class ArticleRepository implements ArticleIRepository
{
    public const _ACTIVE = Article::_ACTIVE;

    public function find() : Paginator
    {
        return Article::whereNull('deleted_at')->paginate(8);
    }

    public function findById(int $id)
    {
        return Article::where('id', $id)
            ->whereNull('deleted_at')
            ->first();
    }

    public function findByAuthor(int $author_id)
    {
        return Article::where('author_id', $author_id)
            ->whereNull('deleted_at')
            ->first();
    }

    public function findByIdByAuthor(int $id, int $author_id)
    {
        return Article::where('id', $id)
            ->where('author_id', $author_id)
            ->whereNull('deleted_at')
            ->first();
    }
    
    public function save(Article $article)
    {
        $article->save();
    }

    public function update(Article $article, $data)
    {
        $article->update($data);
    }

    public function findActives(int $limit)
    {
        return Article::whereNull('deleted_at')
            ->where('active', self::_ACTIVE)
            ->orderByDesc('id')
            ->limit($limit)->get();
    }

    public function findActivesByTypeArticle($article_type_id, $limit)
    {
        return Article::whereNull('deleted_at')
            ->where('active', self::_ACTIVE)
            ->where('article_type_id', $article_type_id)
            ->orderByDesc('id')
            ->limit($limit)
            ->get();
    }
}
