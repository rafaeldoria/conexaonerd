<?php

namespace App\Repositories;

use App\Models\Author;
use App\Repositories\IRepositories\AuthorIRepository;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

class AuthorRepository implements AuthorIRepository
{
    public function find() : Paginator
    {
       return Author::whereNull('deleted_at')->paginate(8);
    }

    public function findByIdByAuthor(int $id)
    {
        return Author::where('id', $id)
            ->where('user_id', Auth::id())
            ->whereNull('deleted_at')
            ->first();
    }

    public function findById(int $id)
    {
        return Author::where('id', $id)
            ->whereNull('deleted_at')
            ->first();
    }

    public function findByUser()
    {
        return Author::where('user_id', Auth::id())
            ->whereNull('deleted_at')
            ->get();
    }
    
    public function save(Author $author)
    {
        return $author->save();
    }

    public function update(Author $author, $data)
    {
        $author->update($data);
    }
}
?>