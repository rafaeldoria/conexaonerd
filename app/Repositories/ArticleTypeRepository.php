<?php

namespace App\Repositories;

use App\Models\ArticleType;
use App\Repositories\IRepositories\ArticleTypeIRepository;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

class ArticleTypeRepository implements ArticleTypeIRepository
{
    public const _ACTIVE = ArticleType::_ACTIVE;

    public function find() : Paginator
    {
        return ArticleType::whereNull('deleted_at')->paginate(8);
    }

    public function findActives() : Collection
    {
        return ArticleType::whereNull('deleted_at')->where('active', self::_ACTIVE)->get();
    }

    public function findBy(int $id) : ArticleType
    {
        return ArticleType::where('id', $id)
            ->whereNull('deleted_at')
            ->first();
    }

    public function save(ArticleType $articleType)
    {
        $articleType->save();
    }

    public function update(ArticleType $articleType, $data)
    {
        $articleType->update($data);
    }
}
