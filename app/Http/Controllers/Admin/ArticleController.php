<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Handlers\ArticleHandler;
use App\Repositories\IRepositories\ArticleIRepository;
use App\Repositories\IRepositories\AuthorIRepository;
use App\Repositories\IRepositories\ArticleTypeIRepository;
use App\Repositories\ArticleRepository;
use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    protected $_repository;
    protected $_articleTypes;
    protected $_repositoryAuthor;
    protected $_handler;

    public function __construct(
        ArticleIRepository $repository, 
        AuthorIRepository $repositoryAuthor,
        ArticleHandler $handler, 
        ArticleTypeIRepository $repositoryArticleTypes
    )
    {
        $this->_repository = $repository;
        $this->_articleTypes = $repositoryArticleTypes->findActives();
        $this->_repositoryAuthor = $repositoryAuthor;
        $this->_handler = $handler;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->_handler->getMyArticles($this->_repository, $this->_repositoryAuthor);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->_handler->create($this->_articleTypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->_handler->store($request, $this->_repository);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->_handler->details($id, $this->_repository);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->_handler->edit($id, $this->_repository, $this->_articleTypes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->_handler->update($request, $id, $this->_repository, $this->_articleTypes);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->_handler->delete($id, $this->_repository);
    }

    public static function findActives(ArticleRepository $repository, ArticleHandler $handler, $limit = 4)
    {
        return $handler->findActives($repository, $limit);
    }
}
