<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Handlers\CapaHandler;
use App\Repositories\CapaRepository;
use App\Repositories\IRepositories\CapaIRepository;
use Illuminate\Http\Request;

class CapaController extends Controller
{
    protected $_repository;
    protected $_handler;

    public function __construct(CapaIRepository $repository, CapaHandler $handler)
    {
        $this->_repository = $repository;
        $this->_handler = $handler;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->_handler->get($this->_repository);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($article_id)
    {
        return $this->_handler->create($article_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->_handler->store($request, $this->_repository, $this->_repository);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->_handler->details($id, $this->_repository);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->_handler->edit($id, $this->_repository);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->_handler->update($request, $id, $this->_repository);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->_handler->delete($id, $this->_repository);
    }

    public static function findActives(CapaRepository $repository, CapaHandler $handler, $limit)
    {
        return $handler->findActives($repository, $limit);
    }
}
