<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Http\Handlers\ArticleTypeHandler;
use App\Http\Handlers\ArticleHandler;
use App\Http\Handlers\FooterHandler;
use App\Http\Controllers\Admin\ArticleTypeController;
use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\CapaController;
use App\Http\Controllers\Admin\FooterController;
use App\Http\Handlers\CapaHandler;
use App\Repositories\ArticleTypeRepository;
use App\Repositories\ArticleRepository;
use App\Repositories\CapaRepository;
use App\Repositories\FooterRepository;
use Illuminate\Support\Arr;

class BlogController extends Controller
{
    protected $_articleRepository;
    protected $_articleHandler;
    protected $_articleTypeRepository;
    protected $_articleTypeHandler;
    protected $_footerRepository;
    protected $_footerHandler;
    protected $_capaRepository;
    protected $_capaHandler;
    protected $_articleTypesActives;
    protected $_footerActives;
    const _TOTAL_INDEX_ARTICLES = 6;
    const _TOTAL_LIST_ARTICLES = 10;

    public function __construct(
        ArticleRepository $articleRepository,
        ArticleHandler $articleHandler,
        ArticleTypeRepository $articleTypeRepository,
        ArticleTypeHandler $articleTypeHandler,
        FooterRepository $footerRepository,
        FooterHandler $footerHandler,
        CapaRepository $capaRepository,
        CapaHandler $capaHandler
        )
    {
        $this->_articleRepository = $articleRepository;
        $this->_articleHandler = $articleHandler;
        $this->_articleTypeRepository = $articleTypeRepository;
        $this->_articleTypeHandler = $articleTypeHandler;
        $this->_footerRepository = $footerRepository;
        $this->_footerRepository = $footerRepository;
        $this->_footerHandler = $footerHandler;
        $this->_capaRepository = $capaRepository;
        $this->_capaHandler = $capaHandler;
        $this->_articleTypesActives = ArticleTypeController::findActives($this->_articleTypeRepository, $this->_articleTypeHandler);
        $footerLimit = $this->_footerRepository::_LIMIT;
        $this->_footerActives = FooterController::findActives($this->_footerRepository, $this->_footerHandler, $footerLimit);
        
    }
    public function index()
    {
        $articlesActives = ArticleController::findActives($this->_articleRepository, $this->_articleHandler, self::_TOTAL_INDEX_ARTICLES);

        $capaLimit = $this->_capaRepository::_LIMIT;
        $capaActives = CapaController::findActives($this->_capaRepository, $this->_capaHandler, $capaLimit);

        return view('blog.index', [
            'articleTypes' => $this->_articleTypesActives,
            'articlesActives' => $articlesActives,
            'footerActives' => $this->_footerActives,
            'capaActives' => $capaActives
        ]);
    }

    public function articles($id)
    {
        $article = $this->_articleRepository->findById($id);
        if(empty($article) || is_null($article)){
            return redirect('/');
        }
        $articleType = $this->_articleTypesActives->firstWhere('id', $article->articleType->id);
        
        return view('blog.article', [
            'article' => $article,
            'articleTypes' => $this->_articleTypesActives,
            'articleType' => $articleType,
            'footerActives' => $this->_footerActives,
        ]);
    }

    public function list($article_type_id)
    {
        $articlesActives = $this->_articleRepository->findActivesByTypeArticle($article_type_id, self::_TOTAL_LIST_ARTICLES);
        $articleType = $this->_articleTypesActives->firstWhere('id', $article_type_id);
        if(empty($articleType) || is_null($articleType)){
            return redirect('/');
        }

        return view('blog.list', [
            'articlesActives' => $articlesActives,
            'articleTypes' => $this->_articleTypesActives,
            'articleType' => $articleType,
            'footerActives' => $this->_footerActives,
        ]);
    }
}
?>