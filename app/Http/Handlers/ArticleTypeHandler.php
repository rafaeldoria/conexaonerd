<?php
namespace App\Http\Handlers;

use App\Http\Controllers\Admin\ArticleTypeController;
use App\Models\ArticleType;
use App\Models\User;
use App\Repositories\IRepositories\ArticleTypeIRepository;
use App\Repositories\ArticleTypeRepository;
use App\Transformers\ArticleTypeTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class ArticleTypeHandler
{
    public const _IS_ADMIN = User::_IS_ADMIN;

    public const _DELETED = 1;
    public const _ACTIVE = ArticleType::_ACTIVE;

    public function get(ArticleTypeIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN){
            $articleTypes = $repository->find();

            return view('admin.articleType.index', [
                'articleTypes' => $articleTypes,
                '_ACTIVE' => self::_ACTIVE
            ]);
        }else{
            return redirect('/dashboard');
        }
    }

    public function create()
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            return view('admin.articleType.create', [
                '_ACTIVE' => self::_ACTIVE
            ]);
        }

        return redirect('/dashboard');
    }

    public function store(Request $request, ArticleTypeIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN){
            $request->validate([
                'title' => 'required|max:50',
                'description' => 'required|max:255',
            ]);

            $articleType = ArticleTypeTransformer::save($request);
            $repository->save($articleType);
            return redirect()->action([ArticleTypeController::class, 'index']);
        }
        
        return redirect('/dashboard');
    }

    public function details($id, ArticleTypeIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN){
            $articleType = $repository->findBy($id);
            if (!is_null($articleType) && !empty($articleType))
                dd($articleType->title, $articleType->description);

            echo "Article is empty";
            exit;
        }
        
        return redirect('/dashboard');
    }

    public function edit($id, ArticleTypeIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN){
            $articleType = $repository->findBy($id);
            if (!is_null($articleType) && !empty($articleType)){
                return view('admin.articleType.create', [
                    'articleType' => $articleType,
                    '_ACTIVE' => self::_ACTIVE
                ]);
            }

            return view('admin.articleType.create', [
                '_ACTIVE' => self::_ACTIVE
            ]);
        }
            
        return redirect('/dashboard');
    }

    public function update(Request $request, $id, ArticleTypeIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN){
            $request->validate([
                'title' => 'required|max:50',
                'description' => 'required|max:255',
            ]);

            $articleType = $repository->findBy($id);
            if (!is_null($articleType) && !empty($articleType)){
                $repository->update($articleType, $request->all());
                if ($request->hasFile('file_path')) {
                    $request->validate([
                        'image' => 'mimes:jpeg,bmp,png'
                    ]);
                    $path = $request->file('file_path')->store('articles');
                    $name = $request->file('file_path')->getClientOriginalName();
                    $articleType->file_path = $path;
                    $articleType->file_name = $name;
                    $repository->save($articleType);
                }
                return redirect()->action([ArticleTypeController::class, 'index']);
            }

            echo "Article is empty";
            exit;
        }

        return redirect('/dashboard');
    }

    public function delete($id, ArticleTypeIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN){
            $articleType = $repository->findBy($id);
            if (!is_null($articleType) && !empty($articleType)){
                $articleType->deleted_at = now();
                $repository->save($articleType);
                return redirect()->action([ArticleTypeController::class, 'index']);
            }
            echo "Type Article is empty";
            exit;
        }
        
        return redirect('/dashboard');
    }

    public static function findActives(ArticleTypeRepository $repository)
    {
        return $repository->findActives($repository);
    }
}
?>