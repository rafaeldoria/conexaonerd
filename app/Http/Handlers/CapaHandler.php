<?php
namespace App\Http\Handlers;

use App\Http\Controllers\Admin\CapaController;
use App\Http\Controllers\Admin\ArticleController;
use App\Models\Capa;
use App\Models\User;
use App\Repositories\ArticleRepository;
use App\Repositories\IRepositories\CapaIRepository;
use App\Repositories\CapaRepository;
use App\Transformers\CapaTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CapaHandler
{
    public const _ACTIVE = Capa::_ACTIVE;

    public const _IS_ADMIN = User::_IS_ADMIN;
    public const _IS_ADMIN_DEFAULT = User::_IS_ADMIN_DEFAULT;

    public function get(CapaIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            $capas = $repository->find();
            return view('admin.capa.index', [
                'capas' => $capas,
                '_ACTIVE' => self::_ACTIVE
            ]);
        } else {
            return redirect('/dashboard');
        }
    }

    public function create($article_id)
    {
        $referer = request()->headers->get('referer');
        $referer = explode('/', $referer)[3];
        $articleRepository = new ArticleRepository();
        $article = $articleRepository->findById($article_id);
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            return view('admin.capa.create', [
                'capa' => '',
                'article' => $article,
                '_ACTIVE' => self::_ACTIVE,
                'referer' => $referer
            ]);
        } else {
            return redirect('/dashboard');
        }
    }

    public function store(Request $request, CapaIRepository $repository)
    {
        $request->validate([
            'file_path' => 'required' 
        ]);
        

        if ($request->hasFile('file_path')) {
            $request->validate([
                'image' => 'mimes:jpeg,bmp,png'
            ]);

            $capa = CapaTransformer::save($request);
            $repository->save($capa);
        }
        if($request->referer == 'capa'){
            return redirect()->action([CapaController::class, 'index']);
        }elseif($request->referer == 'article'){
            return redirect()->action([ArticleController::class, 'index']);
        }
    }

    public function details($id, CapaIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            $capa = $repository->findById($id);
        }else{
            return redirect('/dashboard');
        }
    }

    public function edit($id, CapaIRepository $repository)
    {
        $referer = request()->headers->get('referer');
        $referer = explode('/', $referer)[3];
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            $capa = $repository->findById($id);
            $articleRepository = new ArticleRepository();
            $article = $articleRepository->findById($capa->article->id);
            if((!is_null($capa) && !empty($capa)) && (!is_null($article) && !empty($article))){
                return view('admin.capa.create', [
                    'capa' => $capa,
                    'article' => $article,
                    '_ACTIVE' => self::_ACTIVE,
                    'referer' => $referer
                ]);
            }else{
                return redirect()->action([CapaController::class, 'index']);
            }
        } else {
            return redirect('/dashboard');
        }
    }

    public function update(Request $request, $id, CapaIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            $capa = $repository->findById($id);
            if (!is_null($capa) && !empty($capa)) {
                $repository->update($capa, $request->all());
                if ($request->hasFile('file_path')) {
                    $request->validate([
                        'image' => 'mimes:jpeg,bmp,png'
                    ]);
                    $path = $request->file('file_path')->store('capas');
                    $name = $request->file('file_path')->getClientOriginalName();
                    $capa->file_path = $path;
                    $capa->name = $name;
                    $repository->save($capa);
                }
                if ($request->referer == 'capa') {
                    return redirect()->action([CapaController::class, 'index']);
                } elseif ($request->referer == 'article') {
                    return redirect()->action([ArticleController::class, 'index']);
                }
            }
            echo 'Capa is empty';
            exit;
        }else{
            return redirect('/dashboard');
        }
    }

    public function delete($id,CapaIRepository $repository)
    {
        if(Auth::user()->is_admin){
            $repository->delete($id);
            return redirect()->action([CapaController::class, 'index']);
        }else{
            return redirect('/dashboard');
        }
    }
    
    public function findActives(CapaRepository $repository, $limit)
    {
        return $repository->findActives($limit);
    }
}
?>