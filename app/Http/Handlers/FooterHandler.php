<?php
namespace App\Http\Handlers;

use App\Http\Controllers\Admin\FooterController;
use App\Models\Footer;
use App\Models\User;
use App\Repositories\IRepositories\FooterIRepository;
use App\Repositories\FooterRepository;
use App\Transformers\FooterTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FooterHandler
{
    public const _ACTIVE = Footer::_ACTIVE;

    public const _IS_ADMIN = User::_IS_ADMIN;
    public const _IS_ADMIN_DEFAULT = User::_IS_ADMIN_DEFAULT;

    public function get(FooterIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            $footers = $repository->find();
            return view('admin.footer.index', [
                'footers' => $footers,
                '_ACTIVE' => self::_ACTIVE
            ]);
        } else {
            return redirect('/dashboard');
        }
    }

    public function create()
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            return view('admin.footer.create', [
                'footer' => '',
                '_ACTIVE' => self::_ACTIVE
            ]);
        } else {
            return redirect('/dashboard');
        }
    }

    public function store(Request $request, FooterIRepository $repository)
    {
        $request->validate([
            'url' => 'required|string|max:255',
            'file_path' => 'required' 
        ]);
        

        if ($request->hasFile('file_path')) {
            $request->validate([
                'image' => 'mimes:jpeg,bmp,png'
            ]);

            $footer = FooterTransformer::save($request);
            $repository->save($footer);
        }
        return redirect()->action([FooterController::class, 'index']);
    }

    public function details($id, FooterIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            $footer = $repository->findById($id);
        }else{
            return redirect('/dashboard');
        }
    }

    public function edit($id, FooterIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            $footer = $repository->findById($id);
            return view('admin.footer.create', [
                'footer' => $footer,
                '_ACTIVE' => self::_ACTIVE
            ]);
        } else {
            return redirect('/dashboard');
        }
    }

    public function update(Request $request, $id, FooterIRepository $repository)
    {
        if (Auth::user()->is_admin == self::_IS_ADMIN) {
            $request->validate([
                'url' => 'required|string|max:255'
            ]);
            $footer = $repository->findById($id);
            if (!is_null($footer) && !empty($footer)) {
                $repository->update($footer, $request->all());
                if ($request->hasFile('file_path')) {
                    $request->validate([
                        'image' => 'mimes:jpeg,bmp,png'
                    ]);
                    $path = $request->file('file_path')->store('footers');
                    $name = $request->file('file_path')->getClientOriginalName();
                    $footer->file_path = $path;
                    $footer->name = $name;
                    $repository->save($footer);
                }
                return redirect()->action([FooterController::class, 'index']);
            }
            echo 'Footer is empty';
            exit;
        }else{
            return redirect('/dashboard');
        }
    }

    public function delete($id,FooterIRepository $repository)
    {
        if(Auth::user()->is_admin){
            $footer = $repository->findById($id);
            if (!is_null($footer) && !empty($footer)) {
                $footer->deleted_at = now();
                $repository->save($footer);
                return redirect()->action([FooterController::class, 'index']);
            }
            echo 'Footer is empty';
            exit;
        }else{
            return redirect('/dashboard');
        }
    }
    
    public function findActives(FooterRepository $repository, $limit)
    {
        return $repository->findActives($limit);
    }
}
?>