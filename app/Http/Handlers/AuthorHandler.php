<?php
namespace App\Http\Handlers;

use App\Http\Controllers\Admin\AuthorController;
use App\Models\Author;
use App\Models\User;
use App\Repositories\IRepositories\AuthorIRepository;
use App\Transformers\AuthorTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthorHandler
{
    public const _ACTIVE = Author::_ACTIVE;

    public const _IS_ADMIN = User::_IS_ADMIN;
    public const _IS_ADMIN_DEFAULT = User::_IS_ADMIN_DEFAULT;

    public function get(AuthorIRepository $repository)
    {
        if(Auth::user()->is_admin){
            $authors = $repository->find();
        }else{
            $authors = $repository->findByUser();
        }
        if(!is_null($authors) && !empty($authors)){
            return view('admin.author.index', [
                'authors' => $authors,
                '_ACTIVE' => self::_ACTIVE
            ]);
        }else{
            return view('admin.author.create', [
                'author' => session()->has('author') ? session('author') : '',
                '_IS_ADMIN' => self::_IS_ADMIN,
                '_ACTIVE' => self::_ACTIVE
            ]);
        }
    }

    public function create(AuthorIRepository $repository)
    {
        $author = '';
        if(session()->has('author')){
            $author = session('author');
        }else{
            $author = $repository->findByUser();
        }
        return view('admin.author.create', [
            'author' => $author,
            '_IS_ADMIN' => self::_IS_ADMIN,
            '_ACTIVE' => self::_ACTIVE
        ]);
    }

    public function store(Request $request, AuthorIRepository $repository)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'description' => 'required',
        ]);

        //Sem permissão para ativar Author
        if ($request->active == self::_IS_ADMIN && Auth::user()->is_admin == self::_IS_ADMIN_DEFAULT){
            return response()->view(
                'admin.author.create',
                [
                    'author' => '',
                    'messagem' => 'Insufficient privileges',
                    '_IS_ADMIN' => self::_IS_ADMIN,
                    '_ACTIVE' => self::_ACTIVE
                ],
                500
            );
        }

        $author = AuthorTransformer::save($request);
        $repository->save($author);

        $authorSession = $repository->findByIdByAuthor($author->id);
        if (!is_null($authorSession) && !empty($authorSession)) {
            session(['author' => $authorSession]);
            session('author');
        }
        echo 'Save succeless';  //TODO implementar flash session
        return redirect()->action([AuthorController::class, 'index']);
    }

    public function details($id, AuthorIRepository $repository)
    {
        if(Auth::user()->is_admin){
            $author = $repository->findById($id);
        }else{
            $author = session()->has('author') ? session('author') : $repository->findByIdByAuthor($id);
        }
        if(!is_null($author) && count($author)>0){
            dd($author);
        }

        echo "Not found";
    }

    public function edit($id, AuthorIRepository $repository)
    {
        if(Auth::user()->is_admin){
            $author = $repository->findById($id);
        }else{
            $author = session()->has('author') ? session('author') : $repository->findByIdByAuthor($id);
        }
        return view('admin.author.create', [
            'author' => $author,
            '_IS_ADMIN' => self::_IS_ADMIN,
            '_ACTIVE' => self::_ACTIVE
        ]);
        
    }

    public function update(Request $request, $id, AuthorIRepository $repository)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'description' => 'required',
        ]);

        if ($request->active == self::_IS_ADMIN && Auth::user()->is_admin == self::_IS_ADMIN_DEFAULT) {
            $author = session()->has('author') ? session('author') : $this->_repository->findByIdByAuthor($id);
            return response()->view('admin.author.create', [
                'messagem' => 'Insufficient privileges',
                'author' => $author,
                '_IS_ADMIN' => self::_IS_ADMIN,
                '_ACTIVE' => self::_ACTIVE
            ], 500);
        }

        $author = $repository->findById($id);
        $repository->update($author, $request->all());
        if ($request->hasFile('file_path')) {
            $request->validate([
                'image' => 'mimes:jpeg,bmp,png'
            ]);
            $path = $request->file('file_path')->store('authors');
            $name = $request->file('file_path')->getClientOriginalName();
            $author->file_path = $path;
            $author->file_name = $name;
            $repository->save($author);
        }
        echo 'Updated succeless';

        if(session()->has('author') && session('author')->id == $id){
            $authorSession = $repository->findByIdByAuthor($id);
            if (!is_null($authorSession) && !empty($authorSession)) {
                session(['author' => $authorSession]);
                session('author');
            }
        }
        return redirect()->action([AuthorController::class, 'index']);
    }

    public function delete($id, AuthorIRepository $repository)
    {
        if(Auth::user()->is_admin){
            $author = $repository->findById($id);
            $author->deleted_at = now();
            $repository->save($author);
            if(session()->has('author') && session('author')->id == $id){
                session()->forget('author');
            }
            return redirect()->action([AuthorController::class, 'index']);
        }else{
            $author = session()->has('author') ? session('author') : $this->_repository->findByIdByAuthor($id);
            return response()->view('admin.author.index', [
                'messagem' => 'Insufficient privileges',
                'author' => $author,
                '_IS_ADMIN' => self::_IS_ADMIN,
                '_ACTIVE' => self::_ACTIVE
            ], 500);
        }
    }

}
?>