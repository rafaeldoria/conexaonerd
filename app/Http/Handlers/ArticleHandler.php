<?php
namespace App\Http\Handlers;

use App\Http\Controllers\Admin\ArticleController;
use App\Models\Article;
use App\Models\User;
use App\Repositories\IRepositories\AuthorIRepository;
use App\Repositories\IRepositories\ArticleIRepository;
use App\Repositories\ArticleRepository;
use App\Transformers\ArticleTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ArticleHandler
{
    public const _ACTIVE = Article::_ACTIVE;

    public const _IS_ADMIN = User::_IS_ADMIN;
    public const _IS_ADMIN_DEFAULT = User::_IS_ADMIN_DEFAULT;

    public function getMyArticles(ArticleIRepository $repository, AuthorIRepository $repositoryAuthor)
    {
        $author = '';
        
        if (session()->has('author')) {
            $author = session('author');
        }else{
            $author = $repositoryAuthor->findByUser();
            if (!is_null($author) && !empty($author) && $author->first()) {
                session(['author' => $author->first()]);
                session('author');
            }else{
                $author = '';
                return view('admin.author.create', [
                    'author' => $author,
                    '_IS_ADMIN' => self::_IS_ADMIN,
                    '_ACTIVE' => self::_ACTIVE
                ]);
            }
        }
        if (Auth::user()->is_admin) {
            $articles = $repository->find();
        } else {
            $articles = $repository->findByAuthor($author->id);
        }
        // dd($articles);
        return view('admin.article.index', [
            'articles' => $articles,
            '_ACTIVE' => self::_ACTIVE
        ]);
    }

    public function create($articleTypes)
    {
        if (session()->has('author')) {
            return view('admin.article.create', [
                '_IS_ADMIN' => self::_IS_ADMIN,
                '_ACTIVE' => self::_ACTIVE,
                'articleTypes' => $articleTypes
            ]);
        } else {
            return view('admin.author.create', [
                '_IS_ADMIN' => self::_IS_ADMIN,
                '_ACTIVE' => self::_ACTIVE
            ]);
        }
    }

    public function store(Request $request, ArticleIRepository $repository)
    {
        $request->validate([
            'title' => 'required|max:50',
            'summary' => 'required|max:255',
            'text' => 'required',
        ]);

        if ($request->active == self::_IS_ADMIN && Auth::user()->is_admin == self::_IS_ADMIN_DEFAULT) {
            return response()->view(
                'admin.article.create',
                [
                    'messagem' => 'Insufficient privileges',
                    '_IS_ADMIN' => self::_IS_ADMIN,
                    '_ACTIVE' => self::_ACTIVE
                ],
                500
            );
        }

        if (session()->has('author')) {
            $author = session('author');
            $article = ArticleTransformer::save($request, $author);
            $repository->save($article);
            return redirect()->action([ArticleController::class, 'index']);
        } else {
            return view('admin.author.create');
        }
    }

    public function details($id, ArticleIRepository $repository)
    {
        if (session()->has('author')) {
            $author = session('author');
            $article = $repository->findByIdByAuthor($id, $author->id);
            if (!is_null($article) && !empty($article)) {
                dd($article->title, $article->summary, $article->text);
            } else {
                echo "Article is empty";
                exit;
            }
        } else {
            return view('admin.author.create');
        }
    }

    public function edit($id, ArticleIRepository $repository, $articleTypes)
    {
        if (Auth::user()->is_admin) {
            $article = $repository->findById($id);
        }else{
            if (session()->has('author')) {
                $author = session('author');
                $article = $repository->findByIdByAuthor($id, $author->id);
            }else{
                return view('admin.author.create');
            }    
        }
        if (!is_null($article) && !empty($article)) {
            return view('admin.article.create', [
                'article' => $article,
                '_IS_ADMIN' => self::_IS_ADMIN,
                '_ACTIVE' => self::_ACTIVE,
                'articleTypes' => $articleTypes
            ]);
        } else {
            echo "Article is empty";
            exit;
        }
    }

    public function update(Request $request, $id, ArticleIRepository $repository, $articleTypes)
    {
        $request->validate([
            'title' => 'required|max:50',
            'summary' => 'required|max:255',
            'text' => 'required',
        ]);
        if ($request->active == self::_IS_ADMIN && Auth::user()->is_admin == self::_IS_ADMIN_DEFAULT)
            return response()->view(
                'admin.article.create',
                [
                    'messagem' => 'Insufficient privileges',
                    '_IS_ADMIN' => self::_IS_ADMIN,
                    '_ACTIVE' => self::_ACTIVE,
                    'articleTypes' => $articleTypes
                ],
                500
            );

        if (session()->has('author')) {
            $author = session('author');
            $article = $repository->findById($id, $author->id);
            if (!is_null($article) && !empty($article)) {
                $repository->update($article, $request->all());
                if ($request->hasFile('file_path')) {
                    $request->validate([
                        'image' => 'mimes:jpeg,bmp,png'
                    ]);
                    $path = $request->file('file_path')->store('articles');
                    $name = $request->file('file_path')->getClientOriginalName();
                    $article->file_path = $path;
                    $article->file_name = $name;
                    $repository->save($article);
                }
                return redirect()->action([ArticleController::class, 'index']);
            } else {
                echo "Article is empty";
                exit;
            }
        }
    }

    public function delete($id, ArticleIRepository $repository)
    {
        if (Auth::user()->is_admin) {
            $article = $repository->findById($id);
        } else {
            if (session()->has('author')) {
                $author = session('author');
                $article = $repository->findByIdByAuthor($id, $author->id);
            } else {
                return view('admin.author.create');
            }
        }

        if (!is_null($article) && !empty($article)) {
            $article->deleted_at = now();
            $repository->save($article);
            return redirect()->action([ArticleController::class, 'index']);
        } else {
            echo "Article is empty";
            exit;
        }
    }

    public static function findActives(ArticleRepository $repository, $limit)
    {
        return $repository->findActives($limit);
    }
}
?>