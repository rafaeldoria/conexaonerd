<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class TypeArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('article_types')->insert([
            'title' => 'Geral',
            'description' => 'Todos os tipos de conteúdo Nerd',
            'file_path' => '',
            'file_name' => '',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
