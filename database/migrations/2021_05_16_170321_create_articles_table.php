<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $active_default_value = 0;

            $table->id();
            $table->string('title');
            $table->string('summary');
            $table->mediumtext('text');
            $table->text('file_path');
            $table->text('file_name');
            $table->tinyInteger('active')->default($active_default_value)->comment('0 - Não; 1 - Sim');
            $table->foreignId('article_type_id');
            $table->foreign('article_type_id')->references('id')->on('article_types');
            $table->foreignId('author_id');
            $table->foreign('author_id')->references('id')->on('authors');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
