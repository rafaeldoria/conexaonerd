<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors', function (Blueprint $table) {
            $active_default_value = 0;
            
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->text('description')->nullable();
            $table->text('file_path');
            $table->text('file_name');
            $table->tinyInteger('active')->default($active_default_value)->comment('0 - Não; 1 - Sim');
            $table->foreignId('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
    */
    public function down()
    {
        Schema::dropIfExists('authors');
    }
}
