<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capas', function (Blueprint $table) {
            $active_default_value = 0;

            $table->id();
            $table->text('name');
            $table->text('file_path');
            $table->integer('user_id');
            $table->tinyInteger('active')->default($active_default_value)->comment('0 - Não; 1 - Sim');
            $table->foreignId('article_id');
            $table->foreign('article_id')->references('id')->on('articles');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capas');
    }
}
