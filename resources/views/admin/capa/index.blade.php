@extends('admin.layouts.admintop')

@section('content')

    <div class="container-xl">
        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2 class="indextitle">Manage <b>Capa</b></h2>
                        </div>
                        <div class="col-sm-6">
                            {{-- <a href="{{route('capa.create')}}" class="btn btn-success""><i class="material-icons">&#xE147;</i> <span>New Capa</span></a> --}}
                            {{--  <a href="#deleteArticleModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></a>						  --}}
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><strong>#</strong></th>
                            <th>Name</th>
                            <th>Inserted by</th>
                            <th>Active</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($capas))
                            @foreach ($capas as $capa)
                            <tr>
                                    <td>{{$capa->id}}</td>
                                    <td>{{$capa->name}}</td>
                                    <td>{{$capa->user->username}}</td>
                                    <td>@if ($capa->active == $_ACTIVE) SIM @else NÃO @endif</td>
                                    <td>
                                        <a href="{{route('capa.edit', $capa->id)}}" class="edit" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                        <a href="#deleteCapaModal" class="delete" data-toggle="modal" data-id="{{$capa->id}}"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                    </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <hr>
                <div class="clearfix">
                    <div class="hint-text">Showing <b>{{$capas->count()}}</b> out of <b>{{$capas->total()}}</b> entries</div>
                    <ul class="pagination">
                        <li class="page-item"><a href="{{$capas->previousPageUrl()}}" class="page-link">Previous</a></li>
                        @for ($i = 1;$i <= $capas->lastPage(); $i++)
                        <li class="page-item 
                        @if ($capas->currentPage() == $i)
                            active
                        @endif"><a href="{{$capas->url($i)}}" class="page-link">{{$i}}</a></li>        
                        @endfor
                        <li class="page-item"><a href="{{$capas->nextPageUrl()}}" class="page-link">Next</a></li>
                    </ul>    
                </div>
            </div>
        </div>        
    </div>
    <!-- Delete Modal HTML -->
    <div id="deleteCapaModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="formDeleteCapa" method="POST" action="">
                @csrf
                @if (isset($capa) && $capa != '')
                @method('DELETE')
                @endif
                    <div class="modal-header">						
                        <h4 class="modal-title">Delete Capa</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">					
                        <p>Are you sure you want to delete these Capa?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>  

    <script>
        $('.delete').click(function(event){
            var id = $(this).data('id');
            $(".formDeleteCapa").attr("action", "/capa/" + id);
        })
    </script>
@endsection