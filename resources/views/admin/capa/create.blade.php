@extends('admin.layouts.admintop')

@section('content')
    <div class="container contact-form">
        <div class="contact-image">
            <img src="{{url('/admin/img/logo/conexaonerd.png')}}" alt="ConexãoNerd"/>
        </div>
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form class="" method="POST" enctype="multipart/form-data" action=@if (!isset($capa) || $capa == '') "/capa" @else "/capa/{{$capa->id}}"@endif>
            @csrf
            @if (isset($capa) && $capa != '')
            @method('PUT')
            @endif
            <h2>Capa</h2>
                <input class="hidden" type="text" name="referer" id="referer" value="{{$referer}}">
                <div class="row">
                    <div class="col-md-6">
                        @isset($capa)
                            <div class="form-group">
                                <label for="article">Article Title</label>
                                <input type="text" class="form-control" name="article" id="article" placeholder="article" disabled  
                                value="{{$article->title}}">
                            </div>
                            <div class="form-group hidden">
                                <label for="article_id">Article Id</label>
                                <input type="text" class="form-control" name="article_id" id="article_id" placeholder="article_id"  
                                value="{{$article->id}}">
                            </div>
                        @endisset
                        <div class="form-group">
                            <label for="active">Active</label>
                            <select class="form-control" name="active" id="active">
                                <option value="0">No</option>
                                <option value="1" @if (isset($capa) && $capa != '' && $capa->first() && $capa->active == $_ACTIVE)
                                    selected
                                @endif>Yes</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="file_path">Imagem</label>
                            <small>(750x1000)</small>
                            <input type="file" class="form-control-file" name="file_path" id="file_path"  @if (!isset($capa) && $capa = '') required @endif >
                            
                            <span><h6>Register  @if (isset($capa) && $capa!='' && $capa->first()) <img class="caparegister" src={{asset('storage/'.$capa->file_path)}} alt=""> @endif </h6></span>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btnContactSubmit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
@endsection