@extends('admin.layouts.admintop')

@section('content')
    <div class="container contact-form">
        <div class="contact-image">
            <img src="{{url('/admin/img/logo/conexaonerd.png')}}" alt="ConexãoNerd"/>
        </div>
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form class="" method="POST" enctype="multipart/form-data" action=@if (!isset($footer) || $footer == '') "/footer" @else "/footer/{{$footer->id}}"@endif>
            @csrf
            @if (isset($footer) && $footer != '')
            @method('PUT')
            @endif
            <h2>Footer</h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="url">Url (instagram)</label>
                            <input type="text" class="form-control" name="url" id="url" placeholder="Url (instagram)"  
                            @if (isset($footer) && $footer != '')
                                value="{{$footer->url}}"
                            @else
                                :value="old('url')"
                            @endif>
                        </div>
                        <div class="form-group">
                            <label for="active">Active</label>
                            <select class="form-control" name="active" id="active">
                                <option value="0">No</option>
                                <option value="1" @if (isset($footer) && $footer != '' && $footer->first() && $footer->active == $_ACTIVE)
                                    selected
                                @endif>Yes</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="file_path">Image</label>
                            <input type="file" class="form-control-file" name="file_path" id="file_path" @if (!isset($footer) && $footer = '') required @endif >
                            @if(isset($footer) && $footer != '' && $footer->first())
                                <span><h6>Register <img class="caparegister" src={{asset('storage/'.$footer->file_path)}} alt=""></h6></span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btnContactSubmit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection