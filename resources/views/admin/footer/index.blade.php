@extends('admin.layouts.admintop')

@section('content')

    <div class="container-xl">
        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2 class="indextitle">Manage <b>Footer</b></h2>
                        </div>
                        <div class="col-sm-6">
                            <a href="{{route('footer.create')}}" class="btn btn-success""><i class="material-icons">&#xE147;</i> <span>New Footer</span></a>
                            {{--  <a href="#deleteArticleModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></a>						  --}}
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><strong>#</strong></th>
                            <th>Url</th>
                            <th>Active</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($footers))
                            @foreach ($footers as $footer)
                            <tr>
                                    <td>{{$footer->id}}</td>
                                    <td>{{$footer->url}}</td>
                                    <td>@if ($footer->active == $_ACTIVE) SIM @else NÃO @endif</td>
                                    <td>
                                        <a href="{{route('footer.edit', $footer->id)}}" class="edit" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                        <a href="#deleteFooterModal" class="delete" data-toggle="modal" data-id="{{$footer->id}}"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                    </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <hr>
                <div class="clearfix">
                    <div class="hint-text">Showing <b>{{$footers->count()}}</b> out of <b>{{$footers->total()}}</b> entries</div>
                    <ul class="pagination">
                        <li class="page-item"><a href="{{$footers->previousPageUrl()}}" class="page-link">Previous</a></li>
                        @for ($i = 1;$i <= $footers->lastPage(); $i++)
                        <li class="page-item 
                        @if ($footers->currentPage() == $i)
                            active
                        @endif"><a href="{{$footers->url($i)}}" class="page-link">{{$i}}</a></li>        
                        @endfor
                        <li class="page-item"><a href="{{$footers->nextPageUrl()}}" class="page-link">Next</a></li>
                    </ul>    
                </div>
            </div>
        </div>        
    </div>
    <!-- Delete Modal HTML -->
    <div id="deleteFooterModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="formDeleteFooter" method="POST" action="">
                @csrf
                @if (isset($footer) && $footer != '')
                @method('DELETE')
                @endif
                    <div class="modal-header">						
                        <h4 class="modal-title">Delete Footer</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">					
                        <p>Are you sure you want to delete these Footer?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>  

    <script>
        $('.delete').click(function(event){
            var id = $(this).data('id');
            $(".formDeleteFooter").attr("action", "/footer/" + id);
        })
    </script>
@endsection