@extends('admin.layouts.admintop')

@section('content')

    <div class="container-xl">
        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2 class="indextitle">Manage <b>Article Type</b></h2>
                        </div>
                        <div class="col-sm-6">
                            <a href="{{route('articleType.create')}}" class="btn btn-success""><i class="material-icons">&#xE147;</i> <span>New Article Type</span></a>
                            {{--  <a href="#deleteArticleModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></a>						  --}}
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><strong>#</strong></th>
                            <th>Title</th>
                            <th>Active</th>
                            <th>Background</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($articleTypes))
                            @foreach ($articleTypes as $articleType)
                            <tr>
                                    <td>{{$articleType->id}}</td>
                                    <td>{{$articleType->title}}</td>
                                    <td>@if ($articleType->active == $_ACTIVE) SIM @else NÃO @endif</td>
                                    <td>{{$articleType->file_name}}</td>
                                    <td>
                                        <a href="{{route('articleType.edit', $articleType->id)}}" class="edit" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                        <a href="#deleteArticleTypeModal" class="delete" data-toggle="modal" data-id="{{$articleType->id}}"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                    </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <hr>
                <div class="clearfix">
                    <div class="hint-text">Showing <b>{{$articleTypes->count()}}</b> out of <b>{{$articleTypes->total()}}</b> entries</div>
                    <ul class="pagination">
                        <li class="page-item"><a href="{{$articleTypes->previousPageUrl()}}" class="page-link">Previous</a></li>
                        @for ($i = 1;$i <= $articleTypes->lastPage(); $i++)
                        <li class="page-item 
                        @if ($articleTypes->currentPage() == $i)
                            active
                        @endif"><a href="{{$articleTypes->url($i)}}" class="page-link">{{$i}}</a></li>        
                        @endfor
                        <li class="page-item"><a href="{{$articleTypes->nextPageUrl()}}" class="page-link">Next</a></li>
                    </ul>    
                </div>
            </div>
        </div>        
    </div>
    <!-- Delete Modal HTML -->
    <div id="deleteArticleTypeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="formDeleteArticleType" method="POST" action="">
                @csrf
                @if (isset($articleType) && $articleType != '')
                @method('DELETE')
                @endif
                    <div class="modal-header">						
                        <h4 class="modal-title">Delete Article Type</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">					
                        <p>Are you sure you want to delete these Article Type?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>  

    <script>
        $('.delete').click(function(event){
            var id = $(this).data('id');
            $(".formDeleteArticleType").attr("action", "/articleType/" + id);
        })
    </script>
@endsection