@extends('admin.layouts.admintop')

@section('content')
    <div class="container contact-form">
        <div class="contact-image">
            <img src="{{url('/admin/img/logo/conexaonerd.png')}}" alt="ConexãoNerd"/>
        </div>
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" enctype="multipart/form-data" action=@if (!isset($articleType) || $articleType == '') "/articleType" @else "/articleType/{{$articleType->id}}"@endif>
            @csrf
            @if (isset($articleType) && $articleType != '')
            @method('PUT')
            @endif
            <h2>Article Type</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Title" required 
                        @if (isset($articleType) && $articleType != '')
                            value="{{$articleType->title}}"
                        @else
                            :value="old('title')"
                        @endif>
                    </div>
                    <div class="form-group">
                        <label for="active">Active</label>
                        <select name="active" id="active" class="form-control">
                            <option value="0">No</option>
                            <option value="1" @if (isset($articleType) && $articleType != '' && $articleType->active == $_ACTIVE)
                                selected
                            @endif>Yes</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="file_path">Image</label>
                        <input type="file" class="form-control-file" name="file_path" id="file_path" @if (!isset($articleType) && $articleType = '') required @endif >
                        @if(isset($articleType) && $articleType != '' && $articleType->first())
                            <span><h6>Register <img class="caparegister" src={{asset('storage/'.$articleType->file_path)}} alt=""></h6></span>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btnContactSubmit my-1">Submit</button>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group ">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description" rows="5" required>@if (isset($articleType) && $articleType != ''){{$articleType->description}}@else {!!old('description')!!}@endif</textarea>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection