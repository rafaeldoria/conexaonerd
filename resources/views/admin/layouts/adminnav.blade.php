<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand  mrt-5" href="{{route('dashboard')}}" style="width:100px">
        <img class="logoconexao" src="{{url('/admin/img/logo/conexaonerdmini.png')}}" alt="Image"/>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item {{ (request()->routeIs('author.index')) ? 'active' : '' }}">
            <a class="nav-link" href="{{route('author.index')}}">Author </a>
        </li>
        <li class="nav-item {{ (request()->routeIs('article.index')) ? 'active' : '' }}">
            <a class="nav-link" href="{{route('article.index')}}">Article </a>
        </li>
        @if (Auth::user()->is_admin == 1)
        <li class="nav-item {{ (request()->routeIs('articleType.index')) ? 'active' : '' }}">
            <a class="nav-link" href="{{route('articleType.index')}}">Article Type </a>
        </li>
        <li class="nav-item {{ (request()->routeIs('footer.index')) ? 'active' : '' }}">
            <a class="nav-link" href="{{route('footer.index')}}">Footer (Instagram)</a>
        </li>
        <li class="nav-item {{ (request()->routeIs('capa.index')) ? 'active' : '' }}">
            <a class="nav-link" href="{{route('capa.index')}}">Capa</a>
        </li>
        @endif
    </div>
    <div class="btn-group mr16">
        <li class="form-inline dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->username }}
            </a>
            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                <form method="POST" action="{{ route('logout') }}">
                @csrf
                <a class="dropdown-item" href="route('logout')" onclick="event.preventDefault();this.closest('form').submit();">{{ __('Log out') }}</a>
                </form>
            </div>
        </li>
    </div>
</nav>