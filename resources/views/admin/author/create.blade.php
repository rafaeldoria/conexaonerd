@extends('admin.layouts.admintop')

@section('content')
    <div class="container contact-form">
        <div class="contact-image">
            <img src="{{url('/admin/img/logo/conexaonerd.png')}}" alt="ConexãoNerd"/>
        </div>
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form class="" method="POST" enctype="multipart/form-data" action=@if (!isset($author) || $author == '' || !$author->first()) "/author" @else "/author/{{$author->id}}"@endif>
            @csrf
            @if (isset($author) && $author != '')
            @method('PUT')
            @endif
            <h2>Author</h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name"  
                            @if (isset($author) && $author != '' && $author->first())
                                value="{{$author->first_name}}"
                            @else
                                :value="old('first_name')"
                            @endif>
                        </div>
                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" required
                            @if (isset($author) && $author != '' && $author->first())
                                value="{{$author->last_name}}"
                            @else
                                :value="old('last_name')"
                            @endif>
                        </div>
                        <div class="form-group">
                            <label for="file_path">Avatar</label>
                            <input type="file" class="form-control-file" name="file_path" id="file_path">
                            @if(isset($author) && $author != '' && $author->first() && isset($author->file_path) && $author->file_name != '')
                                <span><h6>Register <img class="caparegister" src={{asset('storage/'.$author->file_path)}} alt=""></h6></span>
                            @endif
                        </div>
                        @if (Auth::user()->is_admin && Auth::user()->is_admin == $_IS_ADMIN)
                        <div class="form-group">
                            <label for="active">Active</label>
                            <select class="form-control" name="active" id="active">
                                <option value="0">No</option>
                                <option value="1" @if (isset($author) && $author != '' && $author->first() && $author->active == $_ACTIVE)
                                    selected
                                @endif>Yes</option>
                            </select>
                        </div>
                        @endif
                        <div class="form-group">
                            <button type="submit" class="btnContactSubmit">Submit</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description" rows="10" required>@if (isset($author) && $author != '' && $author->first()){{$author->description}}@else {!!old('description')!!}@endif</textarea>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection