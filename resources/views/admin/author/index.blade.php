@extends('admin.layouts.admintop')

@section('content')

    <div class="container-xl">
        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2 class="indextitle">Manage <b>Author</b></h2>
                        </div>
                        <div class="col-sm-6">
                            <a href="{{route('author.create')}}" class="btn btn-success""><i class="material-icons">&#xE147;</i> <span>My Author</span></a>
                            {{--  <a href="#deleteAuthorModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></a>						  --}}
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><strong>#</strong></th>
                            <th>Name</th>
                            <th>Avatar</th>
                            <th>Active</th>
                            <th>Username</th>
                            <th>User Email</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($authors))
                            @foreach ($authors as $author)
                            <tr>
                                    <td>{{$author->id}}</td>
                                    <td>{{$author->first_name}}  {{$author->last_name}}</td>
                                    <td>{{$author->file_name}}</td>
                                    <td>@if ($author->active == $_ACTIVE) SIM @else NÃO @endif</td>
                                    <td>{{$author->user->username}}</td>
                                    <td>{{$author->user->email}}</td>
                                    <td>
                                        <a href="{{route('author.edit', $author->id)}}" class="edit" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                        <a href="#deleteAuthorModal" class="delete" data-toggle="modal" data-id="{{$author->id}}"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                    </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <hr>
                <div class="clearfix">
                    <div class="hint-text">Showing <b>{{$authors->count()}}</b> out of <b>{{$authors->total()}}</b> entries</div>
                    <ul class="pagination">
                        <li class="page-item"><a href="{{$authors->previousPageUrl()}}" class="page-link">Previous</a></li>
                        @for ($i = 1;$i <= $authors->lastPage(); $i++)
                        <li class="page-item 
                        @if ($authors->currentPage() == $i)
                            active
                        @endif"><a href="{{$authors->url($i)}}" class="page-link">{{$i}}</a></li>        
                        @endfor
                        <li class="page-item"><a href="{{$authors->nextPageUrl()}}" class="page-link">Next</a></li>
                    </ul>    
                </div>
            </div>
        </div>        
    </div>

    <!-- Delete Modal HTML -->
    <div id="deleteAuthorModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="formDeleteAuthor" method="POST" action="">
                @csrf
                @if (isset($author) && $author != '')
                @method('DELETE')
                @endif
                    <div class="modal-header">						
                        <h4 class="modal-title">Delete Author</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">					
                        <p>Are you sure you want to delete these Author?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $('.delete').click(function(event){
            var id = $(this).data('id');
            $(".formDeleteAuthor").attr("action", "/author/" + id);
        })
    </script>

@endsection
