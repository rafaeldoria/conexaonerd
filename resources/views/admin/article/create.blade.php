@extends('admin.layouts.admintop')
@section('files')
    <!-- Summernote -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<style>
.contact-form h2{
    margin-bottom: 8%;
    margin-top: -10%;
    text-align: center;
    color: #025019;
}

</style>
@endsection

@section('content')
    <div class="container contact-form">
        <div class="contact-image">
            <img src="{{url('/admin/img/logo/conexaonerd.png')}}" alt="ConexãoNerd"/>
        </div>
            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" enctype="multipart/form-data" action=@if (!isset($article) || $article == '') "/article" @else "/article/{{$article->id}}"@endif>
            @csrf
            @if (isset($article))
            @method('PUT')
            @endif
            <h2>Article</h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Article title" 
                            @if (isset($article) && $article != '')
                                value="{{$article->title}}"
                            @else
                                :value="old('title')"
                            @endif>
                        </div>
                        <div class="form-group">
                            <label for="summary">Summary</label>
                            <input type="text" class="form-control" name="summary" id="summary" placeholder="Article summary" 
                            @if (isset($article) && $article != '')
                                value="{{$article->summary}}"
                            @else
                                :value="old('summary')"
                            @endif>
                        </div>
                        <div class="form-group">
                            <label for="file_path">Image (top article 1500x1000)</label>
                            <input type="file" class="form-control-file" name="file_path" id="file_path">
                            @if(isset($article) && $article != '' && $article->first())
                                <span><h6>Register <img class="caparegister" src={{asset('storage/'.$article->file_path)}} alt=""></h6></span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="article_type_id">Article type</label>
                            <select name="article_type_id" id="article_type_id" class="form-control">
                                @foreach ($articleTypes as $articleType)
                                    <option value="{{$articleType->id}}" 
                                        @if ((isset($article) && $article != '') && $article->article_type_id == $articleType->id)
                                            selected
                                        @endif    
                                    >{{$articleType->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if (Auth::user()->is_admin && Auth::user()->is_admin == $_IS_ADMIN)
                        <div class="form-group">
                            <label for="active">Active</label>
                            <select name="active" id="active" class="form-control">
                                <option value="0">No</option>
                                <option value="1" @if (isset($article) && $article != '' && $article->active == $_ACTIVE)
                                    selected
                                @endif>Yes</option>
                            </select>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
							<label for="text">Article text</label>
							<textarea class="form-control" name="text" id="text">@if (isset($article) && $article != ''){{$article->text}} @else {!!old('text')!!} @endif</textarea>
						</div>
                        <div class="form-group">
							<button type="submit" class="btnContactSubmit my-1">Submit</button>
						</div>
					</div>
				</div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    $('#text').summernote({
        placeholder: 'Write your article here',
        tabsize: 2,
        height: 200,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear','fontname']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture', 'video']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
    $('#text').summernote('fontName', 'sans-serif');
    </script>
@endsection