@extends('admin.layouts.admintop')

@section('content')

    <div class="container-xl">
        <div class="table-responsive">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2 class="indextitle">Manage <b>Article</b></h2>
                        </div>
                        <div class="col-sm-6">
                            <a href="{{route('article.create')}}" class="btn btn-success""><i class="material-icons">&#xE147;</i> <span>New Article</span></a>
                            {{--  <a href="#deleteArticleModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></a>						  --}}
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><strong>#</strong></th>
                            <th>Title</th>
                            <th>Type</th>
                            <th>Active</th>
                            <th>Image Top</th>
                            <th>Author</th>
                            <th>Author Email</th>
                            <th class="capaactions">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($articles))
                            @foreach ($articles as $article)
                            <tr>
                                    <td>{{$article->id}}</td>
                                    <td>{{$article->title}}</td>
                                    <td>{{$article->articleType->title}}</td>
                                    <td>@if ($article->active == $_ACTIVE) SIM @else NÃO @endif</td>
                                    <td>{{$article->file_name}}</td>
                                    <td>{{$article->author->first_name}} {{$article->author->last_name}}</td>
                                    <td>{{$article->author->user->email}}</td>
                                    <td>
                                        <a href="{{route('article.edit', $article->id)}}" class="edit" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                        <a href="#deleteArticleModal" class="delete" data-toggle="modal" data-id="{{$article->id}}"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                        @if(isset($article->capa->id) && is_null($article->capa->deleted_at))
                                            <a href="{{route('capa.edit', $article->capa->id)}}" class="editcapa"><i class="material-icons" data-toggle="tooltip" title="EditCapa">image</i></a>
                                        @else
                                            <a href="{{route('article.capa.create', $article->id)}}" class="addcapa"><i class="material-icons" data-toggle="tooltip" title="AddCapa">&#xE147;</i></a>
                                        @endif
                                    </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <hr>
                <div class="clearfix">
                    <div class="hint-text">Showing <b>{{$articles->count()}}</b> out of <b>{{$articles->total()}}</b> entries</div>
                    <ul class="pagination">
                        <li class="page-item"><a href="{{$articles->previousPageUrl()}}" class="page-link">Previous</a></li>
                        @for ($i = 1;$i <= $articles->lastPage(); $i++)
                        <li class="page-item 
                        @if ($articles->currentPage() == $i)
                            active
                        @endif"><a href="{{$articles->url($i)}}" class="page-link">{{$i}}</a></li>        
                        @endfor
                        <li class="page-item"><a href="{{$articles->nextPageUrl()}}" class="page-link">Next</a></li>
                    </ul>    
                </div>
            </div>
        </div>        
    </div>
    <!-- Delete Modal HTML -->
    <div id="deleteArticleModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="formDeleteArticle" method="POST" action="">
                @csrf
                @if (isset($article) && $article != '')
                @method('DELETE')
                @endif
                    <div class="modal-header">						
                        <h4 class="modal-title">Delete Article</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">					
                        <p>Are you sure you want to delete these Article?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>  

    <script>
        $('.delete').click(function(event){
            var id = $(this).data('id');
            $(".formDeleteArticle").attr("action", "/article/" + id);
        })
    </script>
@endsection