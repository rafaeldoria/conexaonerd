@extends('blog.layoutblog.blog')

@section('top')
    @parent
@endsection

@section('content')
    @include('blog.layoutblog.carousel')
    @include('blog.layoutblog.indexbody')
    @include('blog.layoutblog.instagram')
@endsection

@section('footer')
    @parent
@endsection

