@extends('blog.layoutblog.blog')

@section('top')
    @parent
@endsection

@section('content')
    @include('blog.layoutblog.breadcumb')
    @include('blog.layoutblog.articlebody')
    @include('blog.layoutblog.instagram')
@endsection

@section('footer')
    @parent
@endsection
