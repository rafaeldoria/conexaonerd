<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Conexão Nerd</title>

        <link rel="icon" href="{{asset ('blog/img/core-img/favicon.ico')}}">
        <link rel="stylesheet" href="{{ asset('blog/css/style.css')}}" >
        <link rel="stylesheet" href="{{ asset('blog/css/responsive/responsive.css')}}" >

    </head>
    <body>
        @section('top')
            @include('blog.layoutblog.top')
        @show

        @yield('content')

        @section('footer')
            @include('blog.layoutblog.footer')
        @show
    </body>
</html>