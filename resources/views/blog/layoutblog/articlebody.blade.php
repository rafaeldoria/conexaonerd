<section class="single_blog_area section_padding_80">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8">
                <div class="row no-gutters">

                    <div class="col-10 col-sm-11">
                        <div class="single-post">
                            <div class="post-thumb">
                                <img src="{{asset('storage/' . $article->file_path)}}" alt="{{$article->file_name}}">
                            </div>
                            <div class="post-content">
                                <div class="post-meta d-flex">
                                    <div class="post-author-date-area d-flex">
                                        <div class="post-author">
                                        </div>
                                        <div class="post-date">
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <h2 class="post-headline">{{$article->summary}}</h2>
                                </a>
                                <p>{!!($article->text)!!}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="disqus_thread"></div>
            </div>

            <div class="col-12 col-sm-8 col-md-6 col-lg-4">
                <div class="blog-sidebar mt-5 mt-lg-0">
                    <div class="single-widget-area about-me-widget text-center">
                        <div class="widget-title">
                            <h6>About Me</h6>
                        </div>
                        <div class="about-me-widget-thumb">
                            <img src="{{asset('storage/' . $article->author->file_path)}}" alt="">
                        </div>
                        <h4 class="font-shadow-into-light">{{$article->author->first_name}} {{$article->author->last_name}}</h4>
                        <p>{{$article->author->description}}</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    let url = "{!! url()->current() !!}";
    let identifier = "{!! "/article/{$article->id}" !!}";
    var disqus_config = function () {
        this.page.url = url;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = identifier; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    
    (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = 'https://conexaonerd-com-br.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>