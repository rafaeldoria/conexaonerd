<nav class="navbar navbar-expand-lg">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#yummyfood-footer-nav" aria-controls="yummyfood-footer-nav" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars" aria-hidden="true"></i> Menu</button>
    <div class="collapse navbar-collapse justify-content-center" id="yummyfood-footer-nav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            @isset($articleTypes)
                @foreach ($articleTypes as $articleType)
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('blog.list', $articleType->id)}}">{{$articleType->title}}</a>
                    </li>
                @endforeach
            @endisset
        </ul>
    </div>
</nav>