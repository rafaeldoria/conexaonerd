<footer class="footer_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="footer-content">
                    <div class="footer-logo-area text-center">
                        <a href="{{route('blog')}}" class="yummy-logo">Conexao Blog</a>
                    </div>
                    @include('blog.layoutblog.footermenu')
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="copy_right_text text-center">
                    <p>Copyright @2020 All rights reserved | Conexao Nerd</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="{{ asset('blog/js/jquery/jquery-2.2.4.min.js')}}"></script>
<script src="{{ asset('blog/js/bootstrap/popper.min.js')}}"></script>
<script src="{{ asset('blog/js/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{ asset('blog/js/others/plugins.js')}}"></script>
<script src="{{ asset('blog/js/active.js')}}"></script>