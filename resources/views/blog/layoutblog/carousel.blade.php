<section class="welcome-post-sliders owl-carousel">
    
    @if (isset($capaActives) && $capaActives != '' && $capaActives->first())
        @foreach ($capaActives as $capa)
            <div class="welcome-single-slide">
                <a href="{{route('blog.article', $capa->article->id)}}">
                    <img src={{asset('storage/'.$capa->file_path)}} alt="{{$capa->name}}">
                </a>    
                <div class="project_title">
                    <div class="post-date-commnents d-flex">
                        <a href="{{route('blog.article', $capa->article->id)}}">{{$capa->article->created_at->format('F d, Y')}}</a>
                    </div>
                    <a href="{{route('blog.article', $capa->article->id)}}">
                        <h5>{{$capa->article->title}}</h5>
                    </a>
                </div>
            </div>
        @endforeach
    @endif
</section>