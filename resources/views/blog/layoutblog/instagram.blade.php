<div class="instargram_area owl-carousel section_padding_100_0 clearfix" id="portfolio">

    @if (isset($footerActives) && $footerActives != '' && $footerActives->first())
        @foreach ($footerActives as $footer)
            <div class="instagram_gallery_item">
                <img src={{asset('storage/' . $footer->file_path)}} alt="{{$footer->name}}">
                <div class="hover_overlay">
                    <div class="yummy-table">
                        <div class="yummy-table-cell">
                            <div class="follow-me text-center">
                                <a href="{{$footer->url}}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i> Follow me</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>