<section class="archive-area section_padding_80">
    <div class="container">
        <div class="row">

            @if(isset($articlesActives) && $articlesActives->first())
                @foreach ($articlesActives as $articlesActive)
                    <div class="col-12 col-md-6">
                        <div class="single-post wow fadeInUp" data-wow-delay=".4s">
                            <div class="post-thumb">
                                <a href="{{route('blog.article', $articlesActive->id)}}">
                                    <img src="{{asset('storage/' . $articlesActive->file_path)}}" alt="{{$articlesActive->title}}">
                                </a>
                            </div>
                            <div class="post-content">
                                <div class="post-meta d-flex">
                                    <div class="post-author-date-area d-flex">
                                        <div class="post-author">
                                            <a href="#">{{$articlesActive->author->first_name}}</a>
                                        </div>
                                        <div class="post-date">
                                            <a href="{{route('blog.article', $articlesActive->id)}}">{{$articlesActive->created_at->format('F d, Y')}}</a>
                                        </div>
                                    </div>
                                    {{--  <div class="post-comment-share-area d-flex">
                                        <div class="post-favourite">
                                            <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i> 1$i</a>
                                        </div>
                                        <div class="post-comments">
                                            <a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> 12</a>
                                        </div>
                                        <div class="post-share">
                                            <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                        </div>
                                    </div>  --}}
                                </div>
                                <a href="{{route('blog.article', $articlesActive->id)}}">
                                    <h4 class="post-headline">{{$articlesActive->summary}}</h4>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endisset
        </div>
    </div>
</section>
