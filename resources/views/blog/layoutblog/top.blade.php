
<div id="preloader">
    <div class="yummy-load"></div>
</div>

<div class="top_header_area">
    <div class="container">
        <div class="row">
            <div class="mrt-5 logoconexao">
                <img src="{{url('/admin/img/logo/conexaonerdmini.png')}}" alt="ConexãoNerd"/>
            </div>
            <div class="col-5 col-sm-6">
                <div class="top_social_bar">
                    {{--  <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>  --}}
                    <a href="https://twitter.com/ConexaoNerd1" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="https://www.instagram.com/conexaonerd.com.br/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<header class="header_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo_area text-center">
                    <a href="{{route('blog')}}" class="yummy-logo">Conexão Nerd</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                @include('blog.layoutblog.menu')
            </div>
        </div>
    </div>
</header>