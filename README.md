# Conexão Nerd
## Projeto blog Conexão Nerd

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Dependencies

* php 7.4
* composer
* mysql 5.7
* npm

#### Repositorie

- https://gitlab.com/rafaeldoria/conexaonerd

## Installation

```sh
git clone git@gitlab.com:rafaeldoria/conexaonerd.git
```
Dentro de conexaonerd

```sh
composer install
```

copiar .env.example como .env
adicionar no .env arquivos de config do banco

```sh
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisar storage:link
```

```sh
npm install
npm run dev
````
observar warning relativos ao npm instal

```sh
php artisan serve
```

Testar se está rodando em localhost

## Acessar

configurar mailtrap ou outro para verificação de email  
acessar o /register e criar usuario
após validar e-mail
para acessar necessário acessar banco de dados, na tabela user, e alterar campo 
"is_authorized" para 1
Criar author e testar
Para acesso todas funcionalidades alterar na tabela user, campo "is_admin" para 1