<?php

use App\Http\Controllers\Admin\AuthorController;
use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\ArticleTypeController;
use App\Http\Controllers\Admin\FooterController;
use App\Http\Controllers\Admin\CapaController;
use App\Http\Controllers\Blog\BlogController;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#rotas blog
Route::get('/', [BlogController::class, 'index'])->name('blog');
Route::get('/blog/article/{id}', [BlogController::class, 'articles'])->name('blog.article');
Route::get('/blog/list/{article_type_id}', [BlogController::class, 'list'])->name('blog.list');

#rota dashboard
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard')->middleware('verified');

#rotas email
Route::get('/email/verify', function () {
    return view('admin.auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

#rotas admin
Route::resource('author', AuthorController::class)->middleware(['auth'])->middleware('verified');
Route::resource('article', ArticleController::class)->middleware(['auth'])->middleware('verified');
Route::resource('articleType', ArticleTypeController::class)->middleware(['auth'])->middleware('verified');
Route::resource('footer', FooterController::class)->middleware(['auth'])->middleware('verified');
Route::resource('capa', CapaController::class)->middleware(['auth'])->middleware('verified');
Route::get('/article/capa/create/{id}', [CapaController::class, 'create'])->middleware('verified')->name('article.capa.create');

require __DIR__.'/auth.php';
